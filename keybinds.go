package HoggitREPL

import (
	"github.com/c-bata/go-prompt"
)

func Kb_env(livePromptState *LivePrefixState, activeComm *chan string, gamegui_comms *chan string, scripting_comms *chan string) prompt.KeyBind {
	return prompt.KeyBind{
		Key: prompt.Home,
		Fn: func(buffer *prompt.Buffer) {
			if livePromptState.Environment == "Mission" {
				livePromptState.Environment = "GameGUI"
				activeComm = gamegui_comms
			} else if livePromptState.Environment == "GameGUI" {
				livePromptState.Environment = "Mission"
				activeComm = scripting_comms
			}
		},
	}
}
