package.path  = package.path..";.\\LuaSocket\\?.lua;"
package.cpath = package.cpath..";.\\LuaSocket\\?.dll;"
local socket = require('socket')

REPL = {}
net.log("GG REPL IS ALIVE")

-- Setup logging
REPL.logfile = io.open(lfs.writedir().."Logs\\GG_REPL.log", "w")
REPL.log = function(str)
    if REPL.logfile then
        REPL.logfile:write(os.time() .. " - " .. str .. "\n")
        net.log("REPL - " .. os.time() .. " - " .. str)
    end
end

REPL.log("ALIVE")

REPL.callbacks = {}

REPL.resetConnection = function()
    REPL.log("Connection is resetting...")
    if REPL.uconn then REPL.uconn:close() end
    REPL.uconn = socket.udp()
    REPL.uconn:settimeout(0)
    REPL.uconn:setsockname("*", 15302)
    REPL.connection = nil
    REPL.lastListen = DCS.getRealTime()
end

REPL.callbacks.onSimulationStart = function()
    REPL.resetConnection()
    REPL.ingame = true
    REPL.multiplayer = DCS.isMultiplayer()
    REPL.server = DCS.isServer()
end
REPL.log("SIM START CALLBACK DONE")

REPL.callbacks.onSimulationFrame = function()
    if DCS.getRealTime() > REPL.lastListen + 1 then
        REPL.lastListen = DCS.getRealTime()
        if REPL.connection == nil then
            local data, ip, port = REPL.uconn:receivefrom()
            if data == nil then
                return
            end

            if data == "connect" then
                REPL.log("Got connection request from " .. ip)
                REPL.uconn:setpeername(ip, port)
                local success, err = REPL.uconn:send("Connected!\0")

                if success then
                    REPL.connection = {
                        ip = ip,
                        port = port
                    }
                    REPL.log("Completed connection request for " .. ip)
                    REPL.connection.heartbeat = DCS.getRealTime()
                    return
                else
                    REPL.log("Connection request for " .. ip .. " failed: " .. err)
                    REPL.resetConnection()
                end
            end
        end

        if REPL.connection ~= nil then
            if DCS.getRealTime() > REPL.connection.heartbeat + 10 then
                REPL.resetConnection()
                return
            end

            local data, err = REPL.uconn:receive()
            if data == nil then
                if err == "timeout" then return end
                REPL.log(err)
                REPL.resetConnection()
                return
            end

            if data == "HEARTBEAT" then
                REPL.log("Got HEARTBEAT")
                REPL.connection.heartbeat = DCS.getRealTime()
                return
            end

            if data == "GOODBYE" then
                REPL.log("Disconnect request from " .. REPL.connection.ip)
                REPL.resetConnection()
                return
            end

            REPL.log("Got command ".. data .. " from " .. REPL.connection.ip)
            local out = assert(loadstring('return ' .. data))
            if out then
                local success, err = pcall(function()
                    local success, output = pcall(out)
                    if success then
                        if output == nil then output = "nil" end
                        if type(output) == "table" then
                            output = net.lua2json(output)
                        else
                            output = tostring(output)
                        end
                        REPL.uconn:send(os.time() .. " - " .. output .. "\0")
                        return
                    else
                        REPL.log(os.time() .. " - " .. output)
                        REPL.uconn:send(os.time() .. " - " .. output .. "\0")
                        return
                    end
                end)

                if not success then
                    REPL.log(os.time() .. " - " .. err)
                    REPL.uconn:send(os.time() .. " - " .. err .. "\0")
                end
            end
        end
    end
end
REPL.log("FRAME CALLBACK DONE")
DCS.setUserCallbacks(REPL.callbacks)
