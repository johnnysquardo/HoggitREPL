package HoggitREPL

import (
	"fmt"
	"github.com/c-bata/go-prompt"
	"strings"
)

type LivePrefixState struct {
	Environment string
	Address string
}

var livePrefixState LivePrefixState
var scripting_comms, gamegui_comms chan string
var activeComm *chan string
var Env_console *prompt.Prompt
var Main_console *prompt.Prompt
var Remote_addr string

func Main() {
	scripting_comms = make(chan string)
	gamegui_comms = make(chan string)

	livePrefixState = LivePrefixState{
		Environment: "DISCONNECTED",
		Address:     "localhost",
	}

	kb_env := Kb_env(&livePrefixState, activeComm, &gamegui_comms, &scripting_comms)

	Main_console = prompt.New(Executor, Completer,prompt.OptionLivePrefix(updatePrompt))
	Env_console = prompt.New(Env_executor,
		Env_completer,
		prompt.OptionLivePrefix(updatePrompt),
		prompt.OptionAddKeyBind(kb_env))

	Main_console.Run()
}

func Executor(t string) {
	blocks := strings.Fields(t)
	command := blocks[0]
	switch command {
	case "connect":
		if len(blocks) == 2 {
			Remote_addr = blocks[1]
		} else {
			Remote_addr = "127.0.0.1"
		}

		livePrefixState.Address = Remote_addr
		activeComm = &scripting_comms
		livePrefixState.Environment = "Mission"
		Connect(&livePrefixState, activeComm)
		Env_console.Run()
	}
}

func Completer(d prompt.Document) []prompt.Suggest {
	return []prompt.Suggest {
		{Text: "connect", Description: "Connect to remote DCS World instance"},
		{Text: "exit", Description: "Change between the gameGUI and scripting environments"},
	}
}

func updatePrompt() (string, bool){
	return fmt.Sprintf("%s@%s>> ", livePrefixState.Environment, livePrefixState.Address), true
}